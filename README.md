# Ma vie Mon Choix #

## Installation

### Nativescript

Please follow the installation guide at https://docs.nativescript.org/start/quick-setup 

### Clone the repo

    $ git clone git@bitbucket.org:maviemonchoix/mvmc-app.git
    $ cd mvmc-app

### Add Android Platform 

    $ tns platform add android

### Build and run project

    $ tns run android


## Development Workflow

### Pull changes

    $ git pull origin master

### Code

Create a branch to add a new feature, bug fix or any other change:

    $ git checkout -b your-branch-name

Start coding. Write tests as necessary. Then push to origin.

    $ git push origin your-branch-name

### Pull-Request

When your changes are ready for master, make a pull request to master branch and wait for validation

More at info https://www.atlassian.com/git/tutorials/making-a-pull-request