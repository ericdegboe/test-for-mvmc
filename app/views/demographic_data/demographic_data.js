var frameModule = require("ui/frame");
var view = require("ui/core/view");
var pages = require("ui/page");
var dialogsModule = require("ui/dialogs");
var demographicsValues = require("../../data/" + global.lang + "/demographics");
var UserViewModel = require("../../shared/view-models/user-view-model");

var user;
var page;
var mascotImage;

exports.pageLoaded = function pageLoaded(args) {
    user = new UserViewModel();
    page = args.object;
    page.bindingContext = user;
    mascotImage = page.getViewById('mascotImage');
    mascotImage.src = "~/data/mascot/" + user.mascot_id + ".png"
}

exports.genderInput = function () {
    dialogsModule.action(demographicsValues.dialog_gender, demographicsValues.action_cancel, demographicsValues.gender)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("gender", result);
            }
        });
}

exports.departmentInput = function () {
    dialogsModule.action(demographicsValues.dialog_department, demographicsValues.action_cancel, demographicsValues.departments)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("department", result);
            }
        });
}

exports.zoneInput = function () {
    dialogsModule.action(demographicsValues.dialog_zone, demographicsValues.action_cancel, demographicsValues.zone)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("zone", result);
            }
        });
}

exports.speakingLanguageInput = function () {
    dialogsModule.action(demographicsValues.dialog_speaking_language, demographicsValues.action_cancel, demographicsValues.speaking_language)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("speaking_language", result);
            }
        });
}

exports.readingLanguageInput = function () {
    dialogsModule.action(demographicsValues.dialog_reading_language, demographicsValues.action_cancel, demographicsValues.reading_language)
        .then(function (result) {
            if (result !== demographicsValues.action_cancel) {
                user.set("reading_language", result);
            }
        });
}

exports.updateDemographicData = function () {
    //TODO make sure that all the information is provided before to save.
    user.save();
    frameModule.topmost().navigate("views/home/home");
}
