var frameModule = require("ui/frame");  
var page;
var Observable = require("data/observable").Observable;

exports.pageLoaded = function pageLoaded(args) {
  page = args.object;
	page.bindingContext = new Observable();
}


// Event handler for Page "navigatingTo" event attached in main-page.xml

exports.goToMascotte = function() {
  frameModule.topmost().navigate("views/mascotte/mascotte");
};