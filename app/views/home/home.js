var frameModule = require("ui/frame");
var observableModule = require("data/observable");
var userViewModel = require("../../shared/view-models/user-view-model")();
var lessonsData = require("~/data/fr/lessons");
var page;

var lessonList = lessonsData.map(function(obj) {
  return {
    id: obj.id,
    title: obj.title,
    icon: obj.icon,
    progress: userViewModel.getProgressByLesson(obj.id)
  };
});

var pageData = new observableModule.fromObject({
    lessonList: lessonList,
    totalScore: userViewModel.progress.total_score,
    overallProgress: userViewModel.getOverallProgress(),
    mascotSrc: "~/data/mascot/"+userViewModel.mascot_id+".png"
});

exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = pageData;
};
