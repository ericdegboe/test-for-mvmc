var frameModule = require("ui/frame");
var createViewModel = require("../../shared/view-models/hello-view-model").createViewModel;
var userViewModel = require("../../shared/view-models/user-view-model")();
var frameModule = require("ui/frame");

var page;

function onNavigatingTo(args) {
  page = args.object;
  userViewModel.updateAttribute('pseudonym', 'Ananas');
  page.bindingContext = createViewModel();
  page.bindingContext.set('pseudo', userViewModel.pseudonym); 
}
exports.goToMascotte = function() {
  frameModule.topmost().navigate("views/mascotte/mascotte");
};

exports.reversePseudo = function () {
  userViewModel.updateAttribute('pseudonym', userViewModel.pseudonym.split('').reverse().join(''));
  page.bindingContext.set('pseudo', userViewModel.pseudonym);
};

exports.onNavigatingTo = onNavigatingTo;