var frameModule = require("ui/frame");
var createViewModel = require("../../shared/view-models/hello-view-model").createViewModel;
var userViewModel = require("../../shared/view-models/user-view-model")();
var slideContainer;
var activechoice;
var mascots = ["m1","m2"];
var index = 0;

exports.pageLoaded = function pageLoaded(args) {
  page = args.object;
  page.bindingContext = createViewModel();
  page.actionBarHidden = true;
  slideContainer = page.getViewById('mascot-slideContainer');
  activechoice = page.getViewById(mascots[index]);
}

exports.next = function next(args) {
  if(index >= mascots.length){
    return;
  }
  index++;
  activechoice = page.getViewById(mascots[index]);
  activechoice.classList.remove("selected");
  slideContainer.nextSlide();
  page.getViewById('nameField').visibility = 'collapsed';
}

exports.prev = function prev(args) {
  if(index <= 0){
    return;
  }
  index--;
  activechoice = page.getViewById(mascots[index]);
  activechoice.classList.remove("selected");
  slideContainer.previousSlide();
  page.getViewById('nameField').visibility = 'collapsed';
}

exports.choice = function choice(args) {
  activechoice = args.object;
  if (!activechoice.classList.contains("selected")) {
    activechoice.classList.add("selected");
    page.getViewById('nameField').visibility = 'visible';
  }
  else {
    activechoice.classList.remove("selected");
    page.getViewById('nameField').visibility = 'collapsed';
  }
}

exports.choiceSubmit = function choiceSubmit() {
  var values = page.getViewById('mascotName').text, mascotId = activechoice.id;

  if (values === "" || page.getViewById('nameField').visibility != 'visible') {
    activechoice.classList.add("selected");
    page.getViewById('nameField').visibility = 'visible';
  }
  else {
    userViewModel.updateAttribute('mascot_id', mascotId);
    userViewModel.updateAttribute('mascot_name', values);
    frameModule.topmost().navigate("views/demographic_data/demographic_data");
  }
} 
