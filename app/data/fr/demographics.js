module.exports = {
    gender: ["Homme", "Femme"],
    departments: ["Alibori", "Atacora", "Atlantique", "Borgou", "Collines", "Couffo",
        "Donga", "Littoral", "Mono", "Ouémé", "Plateau", "Zou"],
    zone: ["Urbaine", "Rurale"],
    speaking_language: ["Français", "Anglais"],
    reading_language: ["Français", "Anglais"],
    dialog_gender:"Votre genre",
    dialog_department:"Votre département",
    dialog_zone:"Votre zone",
    dialog_speaking_language:"Quelle langue parlez-vous?",
    dialog_reading_language:"Votre langue de lecture",
    action_cancel:"Annuler"
};