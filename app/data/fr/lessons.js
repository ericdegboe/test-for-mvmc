module.exports = [{
  "id": 1,
  "title": "Homme / Femme",
  "icon": "~/data/icons/icon-lesson-1.png",
  "episodes": [{
    "video": {
      "file": "~/data/fr/videos/video-episode-1-1.mp4"
    },
    "dialogue": {
      "id": 1,
      "question": "Aurais tu fais comme Bintou?",
      "answers": [{
          "key": "no",
          "text": "Non, Je ne crois pas"
        },
        {
          "key": "yes",
          "text": "Oui, Je crois"
        },
        {
          "key": "maybe",
          "text": "Je ne sais pas"
        }
      ]
    },
    "thematic": [{
        "title": "Le saviez vous !",
        "file": "image.png",
        "text": "Le coq chante et le jour ...."
      },
      {
        "title": "Le saviez vous !",
        "file": "image.png",
        "text": "Le coq chante et le jour ...."
      }
    ],
    "quiz": [{
        "id": 1,
        "question": "Qui a mangé le poulet ?",
        "answers": [{
            "key": "1",
            "text": "C'est Bintou",
            "value": "false"
          },
          {
            "key": "2",
            "text": "C'est Luc",
            "value": "false"
          },
          {
            "key": "3",
            "text": "C'est Assisba",
            "value": "true"
          }
        ]
      },
      {
        "id": 2,
        "question": "Qui a mangé le poulet ?",
        "answers": [{
            "key": "1",
            "text": "C'est Bintou",
            "value": "false"
          },
          {
            "key": "2",
            "text": "C'est Luc",
            "value": "false"
          },
          {
            "key": "3",
            "text": "C'est Assisba",
            "value": "true"
          }
        ]
      }
    ]
  }]
}];