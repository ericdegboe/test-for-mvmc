var ObservableModule = require("data/observable");
var appSettings = require("application-settings");

module.exports = function () {
  var pointPerSection = 6.25;

  var lessonModel = {
    id: 0,
    episodes: []
  };

  var episodeModel = {
    number: 0,
    video: false,
    dialogue: {
      id: 0,
      answer: {
        key: "",
        text: ""
      }
    },
    thematic: false,
    quiz: []
  };

  var questionModel = {
    id: 0,
    question: "",
    answers: {
      "key": "",
      "text": "",
      "value": ""
    }
  };

  var userModel = new ObservableModule.fromObjectRecursive({
    pseudonym: "",
    age: 0,
    gender: "",
    department: "",
    zone: "",
    speaking_language: "",
    reading_language: "",
    mascot_id: "",
    mascot_name: "",
    progress: {
      total_score: 0,
      lessons: []
    }
  });

  var findByProperty = function (arr, prop, value) {
    return arr.filter(function(obj) {
      return obj[prop] == value;
    })[0];
  };

  var findIndexByProperty = function (arr, prop, value) {
    for(var i = 0; i < arr.length; i++) {
      if(arr[i][prop] == value) {
        return i;
      }
    }
    return -1;
  };

  userModel.remove = function () {
    appSettings.remove('userModel');
  };

  userModel.save = function () {
    appSettings.setString('userModel', JSON.stringify(this._map));
  };

  userModel.load = function () {
    if (!appSettings.hasKey('userModel')) {
      return;
    }
    var data = JSON.parse(appSettings.getString('userModel'));

    for (var prop in data) {
      this.set(prop, data[prop]);
    }
  };

  userModel.updateAttribute = function (prop, value) {
    this.set(prop, value);
    this.save();
  };

  userModel.updateAttributes = function (attributes) {
    for (var prop in  attributes) {
      this.set(prop, attributes[prop]);
    }
    this.save();
  };

  userModel.updateTotalScore = function (score) {
    this.progress.set('total_score', score);
    this.save();
  };

  userModel.addToTotalScore = function (score) {
    this.progress.set('total_score', this.progress.total_score + score);
    this.save();
  };

  userModel.startLesson = function (id) {
    if (findByProperty(this.progress.lessons, 'id', id)) {
      return;
    }

    var lesson = JSON.parse(JSON.stringify(lessonModel));
    lesson.id = id;
    this.progress.lessons.push(lesson);
    this.save();
  };

  userModel.getProgressByLesson = function (lessonId) {
    var lesson = findByProperty(this.progress.lessons, 'id', lessonId);
    var progress = 0;

    for (var i = 0; i < lesson.episodes.length; i++) {
      var episode = lesson.episodes[i];
      progress += episode.video ? pointPerSection: 0;
      progress += episode.dialogue ? pointPerSection : 0;
      progress += episode.thematic ? pointPerSection : 0;
      progress += episode.quiz && episode.quiz.length > 0 ? pointPerSection : 0;
    }

    return progress;
  };

  userModel.getOverallProgress = function () {
    var progress = 0;

    for (var i = 0; i < this.progress.lessons.length; i++) {
      var lessonId = this.progress.lessons[i].id;
      progress += this.getProgressByLesson(lessonId);
    }

    return Math.round(progress / this.progress.lessons.length);
  };

  userModel.startEpisode = function (lessonId, number) {
    var lindex = findIndexByProperty(this.progress._map.lessons, 'id', lessonId);

    if (lindex == -1) {
      return;
    }

    if (findByProperty(this.progress.lessons[lindex].episodes, 'number', number)) {
      return;
    }

    var episode = JSON.parse(JSON.stringify(episodeModel));
    episode.number = number;

    this.progress.lessons[lindex].episodes.push(episode);

    this.save();
    return;
  };

  userModel.completeVideo = function (lessonId, episodeNumber) {
    var lindex = findIndexByProperty(this.progress.lessons, 'id', lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = findByProperty(this.progress.lessons[lindex].episodes, 'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.video = true;
    this.save();
  };

  userModel.answerDialogue = function (lessonId, episodeNumber, dialogueId, answerKey, answerText) {
    var lindex = findIndexByProperty(this.progress.lessons, 'id', lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = findByProperty(this.progress.lessons[lindex].episodes, 'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.dialogue.id = dialogueId;
    episode.dialogue.answer.key = answerKey;
    episode.dialogue.answer.text = answerText;

    this.save();
  };

  userModel.completeThematic = function (lessonId, episodeNumber) {
    var lindex = findIndexByProperty(this.progress.lessons, 'id', lessonId);

    if (lindex == -1) {
      return;
    }

    var episode = findByProperty(this.progress.lessons[lindex].episodes, 'number', episodeNumber);

    if (!episode) {
      return;
    }

    episode.thematic = true;
    this.save();
  };

  userModel.answerQuizQuestion = function (lessonId, episodeNumber, questionId,
    questionLabel, answerKey, answerText, answerValue) {
      var lindex = findIndexByProperty(this.progress.lessons, 'id', lessonId);

      if (lindex == -1) {
        return;
      }

      var episode = findByProperty(this.progress.lessons[lindex].episodes, 'number', episodeNumber);

      if (!episode) {
        return;
      }

      if (findByProperty(episode.quiz, 'id', questionId)) {
        return;
      }

      var q = JSON.parse(JSON.stringify(questionModel));
      q.id = questionId;
      q.question = questionLabel;
      q.answers.key = answerKey;
      q.answers.text = answerText;
      q.answers.value = answerValue;

      episode.quiz.push(q);

      this.save();
  };

  userModel.load();

  return userModel;
};
