var application = require("application");
var i18n = require("./utils/i18n");

global.lang = 'en';

i18n.init(application, global.lang);

application.start({ moduleName: "views/slides/slides" });

/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
